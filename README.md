# Simple Node backend

Really basic node backend to experiment with REST API

## Usage

- Install the dependencies using `yarn install` or `npm install`
- Start the server with `node index.js`, stop it using `Ctrl+C` in the terminal.
- Install other dependencies using `yarn add MY_DEPENDECY` or `npm install MY_DEPENDENCY`

## Docker

When using docker, please be aware that the `node_modules` directory will belong to `root`

- Install the dependencies using `docker-compose run back yarn install` or `docker-compose run back npm install`
- Start the server with `docker-compose up`, stop it using `Ctrl+C` in the terminal.
- Install other dependencies using `docker-compose run back yarn add MY_DEPENDECY` or `docker-compose run back npm install MY_DEPENDENCY`
